qelectrotech (1:0.9-2) unstable; urgency=medium

  * Team upload.
  * Fix debian/watch.

 -- Martin <debacle@debian.org>  Fri, 03 Mar 2023 03:04:08 +0000

qelectrotech (1:0.9-1) unstable; urgency=medium

  * New upstream release.
  * Update patches.

 -- Martin <debacle@debian.org>  Fri, 06 Jan 2023 19:49:02 +0000

qelectrotech (1:0.8.0-3) unstable; urgency=medium

  * Team upload.
  * Re-upload to unstable.

 -- Martin <debacle@debian.org>  Tue, 17 Aug 2021 21:48:20 +0000

qelectrotech (1:0.8.0-2) experimental; urgency=medium

  * Team upload.
  * Fix qelectrotech.install/not-installed (Closes: #985167).
  * Fix some irregularities with coding and language in elements.

 -- Martin <debacle@debian.org>  Sun, 14 Mar 2021 13:03:56 +0000

qelectrotech (1:0.8.0-1) experimental; urgency=medium

  * Team upload.
  * New upstream release.

 -- Martin <debacle@debian.org>  Fri, 12 Mar 2021 00:35:53 +0000

qelectrotech (1:0.7.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.

 -- W. Martin Borgert <debacle@debian.org>  Thu, 12 Sep 2019 20:18:22 +0000

qelectrotech (1:0.7.0~rc2-1) experimental; urgency=medium

  * Team upload.
  * New upstream version: release candidate 2.

 -- W. Martin Borgert <debacle@debian.org>  Sat, 06 Jul 2019 20:03:39 +0000

qelectrotech (1:0.7.0~rc1-1) experimental; urgency=medium

  * Team upload.
  * New upstream version: release candidate 1 (Closes: #929533).

 -- W. Martin Borgert <debacle@debian.org>  Sat, 08 Jun 2019 09:11:38 +0000

qelectrotech (1:0.6.1-1) unstable; urgency=low

  * Team upload.
  * Use debhelper 11 instead of CDBS.
  * Use DEP-14 style git repo.

  [ Laurent trinques ]
  * Remove libs detected automatically from depends.
  * Remove libqt5printsupport5 from recommends.

  [ Denis Briand ]
  * New upstream release (Closes: #888418).
  * Change homepage URI from http to https in debian/control file.
  * Change Vcs-Git URI from git to https in debian/control file.
  * Remove deprecated mimelnk files (Closes: #875519).
  * Bump standards version to 4.1.3
  * Change debian watch URI into https.
  * Refresh qelectrotech.pro patch.
  * Update qelectrotech.install (Use breeze-icons instead of oxygen-icons).
  * Fix FTCBFS: Let dh_auto_configure pass cross flags to qmake.
    Thanks to Helmut Grohne (Closes: #888159).
  * Add LGPL-3 license in debian/copyright for ico/breeze-icons files.
  * Remove these files in debian/copyright they are no longer in the package:
    + ico/oxygen-icons/*
    + ico/oxygen-icons/scalable/mimetypes/hidef/application-x-qet-project.svgz
    + ico/16x16/item_cancel.png
    + ico/16x16/item_copy.png
    + ico/16x16/item_move.png
  * Improve debian/copyright layout.
  * List each spelling error in lintian binary overrides.
  * Fix some spelling errors in binary.
  * Remove dep5-copyright-license-name-not-unique lintian source overrides.
  * Switch debian/copyright header URIs to https
  * Update Source URI in debian/copyright.
  * Rename lintian override name:
    debian-watch-may-check-gpg-signature
    in debian-watch-does-not-check-gpg-signature
  * Bump debhelper compat to 10
  * Remove quilt build dependency (use dpkg-source's embedded functionality)
  * Improve package description and fix english errors in debian/control
  * Git repository migration on salsa.
  * Join the Debian Science Team and update maintainer field.

 -- W. Martin Borgert <debacle@debian.org>  Mon, 01 Oct 2018 20:31:48 +0000

qelectrotech (1:0.5-2) unstable; urgency=medium

  * Add libqt5printsupport5 to recommends.
  * Bump standards to 3.9.8

 -- Denis Briand <debian@denis-briand.fr>  Tue, 26 Apr 2016 21:43:59 +0200

qelectrotech (1:0.5-1) unstable; urgency=low

  * New upstream version using Qt5
  * Override dep5-copyright-license-name-not-unique false positive.
      Same license but another copyright for other files... (see #803634)
  * Laurent Trinques is now co-maintainer and Denis Briand is maintainer
      (thanks to Laurent)
  * Remove debian/menu file. Upstream provides a .desktop file.
      (see #741573)

 -- Denis Briand <debian@denis-briand.fr>  Fri, 27 Nov 2015 23:52:59 +0100

qelectrotech (1:0.4-2) unstable; urgency=low

  * Upload from experimental to unstable.
  * Enable the hardening flags.

 -- Denis Briand <debian@denis-briand.fr>  Fri, 05 Jun 2015 15:23:55 +0200

qelectrotech (1:0.4-1) experimental; urgency=low

  [Laurent Trinques]
  * Add a new 01_pro.diff patch to fix path issue (Closes: #779289).

  [Denis Briand]
  * Update upstream version number to the pristine tar.
      (Upstream will no longer provide specific debian tarball)
  * Bump package version to 1:* (0.4 is lower than 0.40)

 -- Denis Briand <debian@denis-briand.fr>  Thu, 05 Mar 2015 11:19:41 +0100

qelectrotech (0.40-1) experimental; urgency=low

  * New upstream version.
    + Closes: #625906
    + Closes: #652684
    + Closes: #749158
    + Closes: #634079
  * Bump standards to 3.9.6.
  * Update Denis Briand's E-mail.
  * Update Laurent trinques's E-mail.
  * Add libqt4-help in Recommends.
  * Override debian-watch-may-check-gpg-signature lintian warning.
      Upstream doesn't provide gpg signature.
  * Update debian/copyright file to DEP5 machine-readable format.
  * Add strangeplanet icons copyright into debian/copyright file.
  * Remove 02_desktop.diff and 03_qet.diff merged patchs.
  * Remove deprecated 01_pro.diff patch to fix FTBFS if it build twice in a row.
  * Improve debian/watch file to scan the pristine upstream repository.
  * Override spelling-error-in-binary lintian warning.
      There isn't any spelling error. Its french and english technical jargon
  * Add sources/richtext/* copyright into debian/copyright file.

 -- Denis Briand <debian@denis-briand.fr>  Wed, 18 Feb 2015 15:34:41 +0100

qelectrotech (0.22+svn897-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * misc/x-qet-{element, project}.xml:
   - Don't ship in package as they should be managed by
     update-mime-database (Closes: #771134).
  * debian/patches/02_desktop.diff:
   - Allow filenames to be passed as parameters so MIME
     association actually works.

 -- Andrew Starr-Bochicchio <asb@debian.org>  Sun, 30 Nov 2014 21:03:41 -0500

qelectrotech (0.22+svn897-1) unstable; urgency=low

  [Laurent Trinques]
  * New stable release (Closes: #573091)
  * Refresh copyright for new icons
  * Add new translate man
  * Remove 03_qet.diff quilt patch
  * Bump Standards-Version from 3.8.3 to 3.8.4 (no changes needed).

  [Denis Briand]
  * Remove quilt build dependency.
  * Remove debian/README.source

 -- Denis Briand <denis@narcan.fr>  Mon, 15 Mar 2010 09:39:42 +0100


qelectrotech (0.2+svn716-3) unstable; urgency=low

  * Switch to the new source package formats "3.0 (quilt)".
  * Use DEB_BUILD_OPTIONS to chose the number of building processes.
  * Allow Debian Maintainers uploads.
  * Change package section to electronics.

 -- Denis Briand <denis@narcan.fr>  Thu, 12 Nov 2009 13:54:52 +0100

qelectrotech (0.2+svn716-2) unstable; urgency=low

  * Fix FTBFS on armel architecture.
    Thanks to Xavier Guerrin for his patch.
  * Use the patch tagging guidelines.
  * Improve build time on i386 and amd64 multi CPU.
  * Bump Standards-Version to 3.8.3.
  * Add debian/README.source referring to /usr/share/doc/quilt/README.source

 -- Denis Briand <denis@narcan.fr>  Wed, 09 Sep 2009 14:47:11 +0200

qelectrotech (0.2+svn716-1) unstable; urgency=low

  [Laurent Trinques]
  * Initial release (Closes: #456575)

 -- Denis Briand <denis@narcan.fr>  Tue, 30 Jun 2009 14:44:00 +0200

